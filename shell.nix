{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {

    nativeBuildInputs = [
      pkgs.python38
      pkgs.python38Packages.flake8
      pkgs.python38Packages.autopep8
      pkgs.python38Packages.vispy
      pkgs.python38Packages.matplotlib
      pkgs.python38Packages.pyqt5

      pkgs.qt5.full

      pkgs.linuxPackages.bcc
    ];

}
