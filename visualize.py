#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import time
import threading

from shared import Message

from vispy import app, visuals
from vispy.visuals.transforms import STTransform
import numpy as np
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from multiprocessing.connection import Client

NUM_NEW_BUCKETS = 1024
NUM_TRIED_BUCKETS = 256
NUM_ADDR_PER_BUCKET = 64

NUM_ADDR_PER_ADDR_COLUMN = 8

ADDR_PIXEL_SIZE = 6
ADDR_PIXEL_PADDING = 0
ADDR_PIXEL_SIZE_AND_PADDING = ADDR_PIXEL_SIZE + ADDR_PIXEL_PADDING

BUCKET_PIXEL_PADDING = 4
BUCKET_PIXEL_SIZE = NUM_ADDR_PER_ADDR_COLUMN * ADDR_PIXEL_SIZE_AND_PADDING + BUCKET_PIXEL_PADDING

NEW_BUCKETS_PER_BUCKET_COLUMN = 32
TRIED_BUCKETS_PER_BUCKET_COLUMN = 16

PADDING_TOP = 0
PADDING_LEFT = 0

TRIED_X_OFFSET = NEW_BUCKETS_PER_BUCKET_COLUMN * BUCKET_PIXEL_SIZE + PADDING_LEFT + 100


COLOR_RESTORED = (1, 0, 0, 1)
COLOR_NONE = (0, 0, 0, 1)
COLOR_ADDED = (0, 0, 1, 1)

colormap = cm.get_cmap('plasma', 8)

def color_restored(entry):
    return COLOR_RESTORED if entry.restored else COLOR_ADDED

def color_port(entry):
    port = int(entry.addr.split(":")[-1])
    if port != 8333:
        return (port % 255 / 255, port * 3 % 255 / 255, port * 7 % 255 / 255, 1.0)
    else:
        return COLOR_NONE

def color_type(entry):
    COLOR_IPV4 = (1, 0, 0, 0.8)
    COLOR_IPV6 = (0, 1, 0, 0.8)
    COLOR_TORV3 = (0, 0, 1, 0.8)
    COLOR_OTHER = (1, 1, 1, 1)
    if len(entry.addr.split(".")) == 4:
        return COLOR_IPV4
    elif entry.addr.startswith("["):
        return COLOR_IPV6
    elif ".onion" in entry.addr and len(entry.addr) > 60:
        return COLOR_TORV3
    else:
        return COLOR_OTHER
    pass

def color_time(entry):
    curr = int(time.time())
    sec = (curr - entry.time) / 1_000_000
    return colormap(sec)


n_new = NUM_NEW_BUCKETS * NUM_ADDR_PER_BUCKET
n_tried = NUM_TRIED_BUCKETS * NUM_ADDR_PER_BUCKET

new = [None] * n_new
tried = [None] * n_tried

def calc_cords(buckets, addr_per_bucket, buckets_per_addr_column, x_offset = 0):
    table = np.zeros((buckets*addr_per_bucket, 2))
    for bucket in range(buckets):
        for pos in range(addr_per_bucket):
            row = int(bucket / buckets_per_addr_column)
            column = bucket % buckets_per_addr_column
            x = (column * BUCKET_PIXEL_SIZE + pos % NUM_ADDR_PER_ADDR_COLUMN * ADDR_PIXEL_SIZE_AND_PADDING) + x_offset
            y = ((row * BUCKET_PIXEL_SIZE) + ADDR_PIXEL_SIZE_AND_PADDING * int(pos / NUM_ADDR_PER_ADDR_COLUMN))
            i = bucket * NUM_ADDR_PER_BUCKET + pos
            table[i] = x, y
    return table

new_coords = calc_cords(NUM_NEW_BUCKETS, NUM_ADDR_PER_BUCKET, NEW_BUCKETS_PER_BUCKET_COLUMN)
tried_coords = calc_cords(NUM_TRIED_BUCKETS, NUM_ADDR_PER_BUCKET, TRIED_BUCKETS_PER_BUCKET_COLUMN, TRIED_X_OFFSET)

class Canvas(app.Canvas):
    def __init__(self):
        app.Canvas.__init__(self, title='Addrman', fullscreen= True, keys='interactive')

        self.markers_new = visuals.MarkersVisual()
        self.markers_new.antialias = 0
        self.markers_new.transform = STTransform()

        self.markers_tried = visuals.MarkersVisual()
        self.markers_tried.antialias = 0
        self.markers_tried.transform = STTransform()

        #self.measure_fps()

        self.new_colors = np.full((n_new, 4), COLOR_NONE, dtype=np.float32)
        self.tried_colors = np.full((n_tried, 4), COLOR_NONE, dtype=np.float32)

        self._timer = app.Timer(0.05, connect=self.update, start=True)

        self.text = visuals.TextVisual('', bold=True, pos=(0., 0.))
        self.update_text()
        self.show()

    def on_draw(self, event):
        self.context.clear(color='gray')
        self.markers_new.set_data(new_coords, face_color=self.new_colors, symbol="square", size = 4, edge_width = 0.0, scaling=True)
        self.markers_new.draw()
        self.markers_tried.set_data(tried_coords, face_color=self.tried_colors, symbol="square", size = 4, edge_width = 0.0, scaling=True)
        self.markers_tried.draw()
        self.update_text()
        #self.text.draw()

    def on_new_changed(self, bucket, pos):
        i = bucket * NUM_ADDR_PER_BUCKET + pos
        self.new_colors[i] = color_type(new[i])

    def on_tried_changed(self, bucket, pos):
        i = bucket * NUM_ADDR_PER_BUCKET + pos
        self.tried_colors[i] = color_type(tried[i])

    def on_mouse_wheel(self, event):
        """Use the mouse wheel to zoom."""
        self.markers_new.transform.zoom((1.25**event.delta[1],)*2, center=event.pos)
        self.markers_tried.transform.zoom((1.25**event.delta[1],)*2, center=event.pos)
        self.update()

    def on_mouse_move(self, event):
        if event.is_dragging:
            x0, y0 = event.press_event.pos
            x1, y1 = event.last_event.pos
            x, y = event.pos
            dx, dy = x - x1, y - y1
            self.markers_new.transform.move((dx, dy, 0., 0.))
            self.markers_tried.transform.move((dx, dy, 0., 0.))

    def on_resize(self, event):
        # Set canvas viewport and reconfigure visual transforms to match.
        vp = (0, 0, self.physical_size[0], self.physical_size[1])
        self.context.set_viewport(*vp)
        self.markers_new.transforms.configure(viewport=vp, canvas=self)
        self.markers_tried.transforms.configure(viewport=vp, canvas=self)

    def update_text(self):
        self.text.text = "addrman observer"
        self.text.anchors = ("left", "top")
        self.text.font_size = 9
        self.text.pos = 4, self.size[1]

def receive_thread(conn, canvas):
    while True:
        msg = conn.recv()
        handle_message(msg, canvas)

def handle_message(msg, canvas):
    if msg[0] == Message.ADD_TO_NEW:
        i = msg[1].bucket * NUM_ADDR_PER_BUCKET + msg[1].pos
        new[i] = msg[1]
        canvas.on_new_changed(msg[1].bucket, msg[1].pos)
    elif msg[0] == Message.RESTORE_NEW:
        i = msg[1].bucket * NUM_ADDR_PER_BUCKET + msg[1].pos
        new[i] = msg[1]
        canvas.on_new_changed(msg[1].bucket, msg[1].pos)
    elif msg[0] == Message.RESTORE_TRIED:
        i = msg[1].bucket * NUM_ADDR_PER_BUCKET + msg[1].pos
        tried[i] = msg[1]
        canvas.on_tried_changed(msg[1].bucket, msg[1].pos)

if __name__ == '__main__':

    conn = Client(("localhost", 18329))

    canvas = Canvas()
    if sys.flags.interactive != 1:
        x = threading.Thread(target=receive_thread, args=(conn, canvas, ))
        x.start()

        app.run()

