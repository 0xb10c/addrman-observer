# addrman-observer

Usage:

1. Start `bitcoind` with addrman tracepoints
2. Then, start `daemon.py` with root privileges supplying the path to `bitcoind` as first and the `pid` of `bitcoind` started in 1. as second argument
  - e.g. `sudo python3 daemon.py /path/to/bitcoind $(pidof bitcoind)`
3. run `python3 visualization.py`

The daemon and visualization are split into two processes on purpose.
Hooking into eBPF tracepoints requires root privileges, but there is
no need to run the visualization as root.
