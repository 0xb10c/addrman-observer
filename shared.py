from enum import Enum

class Message(Enum):
    ADD_TO_NEW = 1
    MOVE_NEW_TO_TRIED = 2
    RESTORE_NEW = 3
    RESTORE_TRIED = 4

class Addr:
    id = 0
    addr = ""
    source = ""
    bucket = 0
    pos = 0
    time = 0
    restored = False
    source_group = None

    def __init__(self, id, addr, source, bucket, pos, time, source_group, restored = False):
        self.id = id
        self.addr = addr
        self.source = source
        self.bucket = bucket
        self.pos = pos
        self.time = time
        self.restored = restored
        self.source_group = source_group

    def __str__(self):
        return f"Addr(id={self.id}, addr={self.addr}, src={self.source}, bucket={self.bucket}, pos={self.pos}, time={self.time}, source_group={self.source_group}, restored={self.restored}"
