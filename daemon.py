#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from multiprocessing.connection import Listener

from bcc import BPF, USDT

from shared import Addr, Message

EBPF_PROGRAM = program = """
#include <uapi/linux/ptrace.h>

// Tor v3 addresses are 62 chars + 6 chars for the port (':12345').
#define MAX_ADDR_LENGTH 62 + 6
#define MAX_GROUP_LENGTH 8

struct add_to_new
{
    u32     id;
    char    addr[MAX_ADDR_LENGTH];
    char    source[MAX_ADDR_LENGTH];
    u32     bucket;
    u32     pos;
    u32     time;
    u64     source_group;
};

BPF_PERF_OUTPUT(addr_added_to_new);
BPF_PERF_OUTPUT(new_to_tried);
BPF_PERF_OUTPUT(restored_to_new);
BPF_PERF_OUTPUT(restored_to_tried);

int trace_add_to_new(struct pt_regs *ctx) {
    struct add_to_new n = {};

    bpf_usdt_readarg(1, ctx, &n.id);
    bpf_usdt_readarg_p(2, ctx, &n.addr, MAX_ADDR_LENGTH);
    bpf_usdt_readarg_p(3, ctx, &n.source, MAX_ADDR_LENGTH);
    bpf_usdt_readarg(4, ctx, &n.bucket);
    bpf_usdt_readarg(5, ctx, &n.pos);
    bpf_usdt_readarg(6, ctx, &n.time);
    bpf_usdt_readarg_p(7, ctx, &n.source_group, MAX_GROUP_LENGTH);

    addr_added_to_new.perf_submit(ctx, &n, sizeof(n));
    return 0;
};


int trace_restored_to_new(struct pt_regs *ctx) {
    struct add_to_new n = {};

    bpf_usdt_readarg(1, ctx, &n.id);
    bpf_usdt_readarg_p(2, ctx, &n.addr, MAX_ADDR_LENGTH);
    bpf_usdt_readarg_p(3, ctx, &n.source, MAX_ADDR_LENGTH);
    bpf_usdt_readarg(4, ctx, &n.bucket);
    bpf_usdt_readarg(5, ctx, &n.pos);
    bpf_usdt_readarg(6, ctx, &n.time);
    bpf_usdt_readarg_p(7, ctx, &n.source_group, MAX_GROUP_LENGTH);

    restored_to_new.perf_submit(ctx, &n, sizeof(n));
    return 0;
};

int trace_restored_to_tried(struct pt_regs *ctx) {
    struct add_to_new n = {};

    bpf_usdt_readarg(1, ctx, &n.id);
    bpf_usdt_readarg_p(2, ctx, &n.addr, MAX_ADDR_LENGTH);
    bpf_usdt_readarg_p(3, ctx, &n.source, MAX_ADDR_LENGTH);
    bpf_usdt_readarg(4, ctx, &n.bucket);
    bpf_usdt_readarg(5, ctx, &n.pos);
    bpf_usdt_readarg(6, ctx, &n.time);
    bpf_usdt_readarg_p(7, ctx, &n.source_group, MAX_GROUP_LENGTH);

    restored_to_tried.perf_submit(ctx, &n, sizeof(n));
    return 0;
};


int trace_new_to_tried(struct pt_regs *ctx) {
    struct add_to_new n = {};

    bpf_usdt_readarg(1, ctx, &n.id);
    bpf_usdt_readarg_p(2, ctx, &n.addr, MAX_ADDR_LENGTH);
    bpf_usdt_readarg_p(3, ctx, &n.source, MAX_ADDR_LENGTH);
    bpf_usdt_readarg(4, ctx, &n.bucket);
    bpf_usdt_readarg(5, ctx, &n.pos);
    bpf_usdt_readarg(6, ctx, &n.time);
    bpf_usdt_readarg_p(7, ctx, &n.source_group, MAX_GROUP_LENGTH);

    new_to_tried.perf_submit(ctx, &n, sizeof(n));
    return 0;
};
"""

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("USAGE:", sys.argv[0], "path/to/bitcoind", "pid")
        exit()
    path = sys.argv[1]
    pid = sys.argv[2]

    bitcoind_with_usdts = USDT(path=str(path), pid=int(pid))

    bitcoind_with_usdts.enable_probe(probe="add_to_new", fn_name="trace_add_to_new")
    bitcoind_with_usdts.enable_probe(probe="new_to_tried", fn_name="trace_new_to_tried")
    bitcoind_with_usdts.enable_probe(probe="restore_to_new", fn_name="trace_restored_to_new")
    bitcoind_with_usdts.enable_probe(probe="restore_to_tried", fn_name="trace_restored_to_tried")
    bpf = BPF(text=program, usdt_contexts=[bitcoind_with_usdts])

    def handle_add_to_new(_, data, size):
        event = bpf["addr_added_to_new"].event(data)
        addr = Addr(event.id, event.addr.decode("utf-8"), event.source.decode("utf-8"), event.bucket, event.pos, event.time, event.source_group)
        conn.send((Message.ADD_TO_NEW, addr))

    def handle_new_to_tried(_, data, size):
        event = bpf["new_to_tried"].event(data)
        addr = Addr(event.id, event.addr.decode("utf-8"), event.source.decode("utf-8"), event.bucket, event.pos, event.time, event.source_group)
        conn.send((Message.MOVE_NEW_TO_TRIED, addr))

    def handle_restore_to_new(_, data, size):
        event = bpf["restored_to_new"].event(data)
        addr = Addr(event.id, event.addr.decode("utf-8"), event.source.decode("utf-8"), event.bucket, event.pos, event.time, event.source_group, True)
        print(addr)
        conn.send((Message.RESTORE_NEW, addr))

    def handle_restore_to_tried(_, data, size):
        event = bpf["restored_to_tried"].event(data)
        addr = Addr(event.id, event.addr.decode("utf-8"), event.source.decode("utf-8"), event.bucket, event.pos, event.time, event.source_group, True)
        conn.send((Message.RESTORE_TRIED, addr))

    bpf["addr_added_to_new"].open_perf_buffer(handle_add_to_new, 128)
    bpf["new_to_tried"].open_perf_buffer(handle_new_to_tried, 128)
    bpf["restored_to_new"].open_perf_buffer(handle_restore_to_new, 2048)
    bpf["restored_to_tried"].open_perf_buffer(handle_restore_to_tried, 2048)

    listener = Listener(("localhost", 18329))
    print("Waiting for visualization.py...")
    conn = listener.accept()
    print('Connection accepted from', listener.last_accepted)

    if sys.flags.interactive != 1:
        while True:
            try:
                bpf.perf_buffer_poll()
            except KeyboardInterrupt:
                conn.close()
                exit()